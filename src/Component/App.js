import React from "react";
import Layout from "./Layout/Layout";
import BurgerBuilder from "../Container/BurgerBuilder/BurgerBuilder";
import OrderHistory from "../Container/OrderHistory/OrderHistory";
import Checkout from "../Container/Checkout/Checkout";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Layout>
          <Switch>
            <Route path="/builder/checkout" component={Checkout} />
            <Route path="/builder" component={BurgerBuilder} />
            <Route path="/orders" component={OrderHistory} />
            <Redirect from="/" to="builder" exact component={BurgerBuilder} />
          </Switch>
        </Layout>
      </div>
    </BrowserRouter>
  );
}

export default App;
