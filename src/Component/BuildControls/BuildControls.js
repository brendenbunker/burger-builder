import React from "react";
import IngredientController from "./IngredientController/IngredientController";
import classes from "./BuildControls.module.css";
import Counter from "../Burger/Counter/Counter";

const BuildControls = props => {
  let bottomBanner = null;
  if (!props.orderable) {
    bottomBanner = (
      <p style={{ color: "white" }}>
        <strong>Start Adding Ingredient</strong>
      </p>
    );
  } else {
    bottomBanner = (
      <>
        <Counter label="Calories" total={props.calories} color="white" />
        <Counter label="Price" total={props.price} color="white" />
      </>
    );
  }

  return (
    <>
      <div className={classes.burgerStats}>{bottomBanner}</div>
      <div className={classes.buildControls}>
        {props.ingredients
          .filter(item => item.visible)
          .map(ingredient => {
            return (
              <IngredientController
                key={ingredient.id}
                ingredient={ingredient}
                updateQuantity={props.updateQuantity}
              />
            );
          })}
        <button
          className={classes.orderButton}
          disabled={!props.orderable}
          onClick={props.order}
        >
          Order Now
        </button>
      </div>
    </>
  );
};

export default BuildControls;
