import React from "react";
import classes from "./IngredientController.module.css";

const IngredientController = props => (
  <div className={classes.ingredientController}>
    <p className={classes.ingredientLabel}>
      {props.ingredient.name.replace(/^\w/, c => c.toUpperCase())}
    </p>
    <button
      className={classes.buttonStyle}
      onClick={() => props.updateQuantity(props.ingredient.id, -1)}
      disabled={props.ingredient.quantity <= 0}
    >
      -
    </button>
    <p>{props.ingredient.quantity}</p>
    <button
      className={classes.buttonStyle}
      onClick={() => props.updateQuantity(props.ingredient.id, 1)}
    >
      +
    </button>
  </div>
);

export default IngredientController;
