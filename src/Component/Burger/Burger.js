import React from "react";
import Ingredient from "./Ingredient/Ingredient";
import uuid from "uuid/v4";
import classes from "./Burger.module.css";

const Burger = props => {
  let ingredentsToRender = [];
  props.ingredients.forEach(ingredient => {
    for (var i = 0; i < ingredient.quantity; i++) {
      ingredentsToRender.push(
        <Ingredient key={uuid()} ingredient={ingredient.name} />
      );
    }
  });

  return (
    <div className={classes.burgerContainer}>
      <div className={classes.burger}>{ingredentsToRender}</div>
    </div>
  );
};

export default Burger;
