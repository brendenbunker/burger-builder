import React from "react";
//import classes from "./Counter.module.css";

const Counter = props => {
  let newStyle = { color: props.color };
  return (
    <p style={newStyle}>
      <strong>
        {props.label}: {props.label === "Price" ? "$" : ""}
        {props.total.toFixed(2)}
      </strong>
    </p>
  );
};

export default Counter;
