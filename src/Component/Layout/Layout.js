import React, { Component } from "react";
import classes from "./Layout.component.css";
import Toolbar from "../Navigation/Toolbar/Toolbar";

class Layout extends Component {
  state = {
    showSideDrawer: false
  };

  sideDrawerClosed = () => {
    this.setState({ showSideDrawer: false });
  };

  openSideDrawer = () => {
    this.setState({ showSideDrawer: true });
  };

  render() {
    return (
      <>
        <Toolbar
          closed={this.sideDrawerClosed}
          open={this.openSideDrawer}
          show={this.state.showSideDrawer}
        />
        <div>SideDrawer, Backdrop</div>
        <main className={classes.Content}>{this.props.children}</main>
      </>
    );
  }
}

export default Layout;
