import React from "react";

import classes from "./NavigationItems.module.css";
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = props => (
  <ul className={classes.NavigationItems}>
    <NavigationItem link="/builder">Burger Builder</NavigationItem>
    <NavigationItem link="/orders">Order History</NavigationItem>
  </ul>
);

export default NavigationItems;
