import React from "react";

import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import Backdrop from "../../UI/Backdrop/Backdrop";

import classes from "./SideDrawer.module.css";

const SideDrawer = props => {
  let dynClass = [classes.SideDrawer];
  if (props.show) {
    dynClass.push(classes.Open);
  } else {
    dynClass.push(classes.Close);
  }
  return (
    <>
      <Backdrop show={props.show} clicked={props.closed} />
      <button className={classes.Reveal} onClick={props.open}>
        ≡
      </button>
      <div className={dynClass.join(" ")}>
        <button className={classes.CloseX} onClick={props.closed}>
          X
        </button>
        <div className={classes.Logo}>
          <Logo />
        </div>
        <nav>
          <NavigationItems />
        </nav>
      </div>
    </>
  );
};

export default SideDrawer;
