import React from "react";

import classes from "./Toolbar.module.css";
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import SideDrawer from "../SideDrawer/SideDrawer";

const Toolbar = props => (
  <header className={classes.Toolbar}>
    <SideDrawer closed={props.closed} show={props.show} open={props.open} />
    <nav className={classes.ToolbarNav}>
      <NavigationItems />
    </nav>
    <div className={classes.Logo}>
      <Logo />
    </div>
  </header>
);

export default Toolbar;
