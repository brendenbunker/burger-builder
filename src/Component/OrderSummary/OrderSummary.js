import React from "react";
import uuid from "uuid/v4";

import classes from "./OrderSummary.module.css";
import Counter from "../Burger/Counter/Counter";

const OrderSummary = props => {
  return (
    <>
      <h3>Your Order</h3>
      <p>You decided on the following Ingredients:</p>
      <ul>
        {props.ingredients
          .filter(i => i.visible && i.quantity > 0)
          .map(ingredient => {
            return (
              <li key={uuid()}>
                {ingredient.name.replace(/^\w/, c => c.toUpperCase())} x
                {ingredient.quantity}
              </li>
            );
          })}
      </ul>
      <Counter label="Price" total={props.price} color="black" />
      <p>Continue to checkout?</p>
      <div className={classes.ButtonContainer}>
        <button onClick={props.orderBurger}>Yes</button>
        <button onClick={props.cancel}>No</button>
      </div>
    </>
  );
};

export default OrderSummary;
