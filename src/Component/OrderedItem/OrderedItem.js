import React from "react";
import Counter from "../Burger/Counter/Counter";
//import Burger from "../Burger/Burger";
import classes from "./OrderedItem.module.css";

const OrderedItem = props => {
  let date = new Date(props.date);

  let dateStr = date.toLocaleString("default", {
    month: "short",
    day: "numeric",
    year: "numeric",
    hour: "numeric",
    minute: "numeric"
  });

  return (
    <div className={classes.OrderedItem}>
      <h1>{dateStr}</h1>
      <h3>Ingredients</h3>
      <ul>
        {props.ingredients
          .filter(ingredient => ingredient.visible)
          .map(ingredient => (
            <li key={ingredient.id + date}>
              {ingredient.name} * {ingredient.quantity}
            </li>
          ))}
      </ul>
      <Counter label="Calories" total={props.calories} />
      <Counter label="Price" total={props.price} />
    </div>
  );
};

export default OrderedItem;
