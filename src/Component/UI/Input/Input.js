import React from "react";
import classes from "./Input.module.css";

const Input = props => (
  <div className={classes.FormRow}>
    <label>{props.name}</label>
    <input {...props} />
  </div>
);

export default Input;
