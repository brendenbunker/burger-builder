import React from "react";
import modules from "./Spinner.module.css";

const Spinner = () => {
  return <div className={modules.loader}>Loading...</div>;
};

export default Spinner;
