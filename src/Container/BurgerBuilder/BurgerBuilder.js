import React, { Component } from "react";
import Burger from "../../Component/Burger/Burger";
import BuildControls from "../../Component/BuildControls/BuildControls";
import Modal from "../../Component/UI/Modal/Modal";
import OrderSummary from "../../Component/OrderSummary/OrderSummary";
import axios from "../../axiosOrderer";
import Spinner from "../../Component/UI/Spinner/Spinner";
import withErrorHandler from "../../HOC/withErrorHandler/withErrorHandler";
import { Redirect } from "react-router-dom";

class BurgerBuilder extends Component {
  componentDidMount = () => {
    axios.get("/ingredients.json").then(response => {
      this.setState({
        ingredients: response.data
      });
    });
  };

  state = {
    canOrder: false,
    totalIngredients: 0,
    price: 4.0,
    calories: 0.0,
    ordered: false,
    loading: false,
    orderCompleted: false,
    ingredients: []
  };

  orderBurger = () => {
    this.setState({ orderCompleted: true });
  };

  updateQuantity = (id, value) => {
    this.setState((oldState, props) => {
      let ingredients = [...oldState.ingredients];
      let ingredientIndex = ingredients.findIndex(
        ingredient => ingredient.id === id
      );
      let ingredient = { ...ingredients[ingredientIndex] };
      ingredient.quantity += value;

      let newSummary = this.updateOrderSummary(oldState, ingredient, value);

      ingredients[ingredientIndex] = ingredient;
      return {
        price: newSummary.price,
        calories: newSummary.calories,
        canOrder: newSummary.canOrder,
        totalIngredients: newSummary.totalIngredients,
        ingredients: ingredients
      };
    });
  };

  updateOrderSummary = (oldState, ingredient, value) => {
    let newQuantity = oldState.totalIngredients + value;
    if (ingredient.quantity < 0) {
      ingredient.quantity = 0;
    }

    let newPrice = oldState.price + value * ingredient.price;
    if (newPrice <= 4.0) {
      newPrice = 4.0;
    }

    let newCalories = oldState.calories + value * ingredient.calories;
    if (newCalories <= 0.0) {
      newPrice = 0.0;
    }

    let canOrder = false;
    if (newQuantity <= 0) {
      newQuantity = 0;
      canOrder = false;
    } else {
      canOrder = true;
    }
    return {
      price: newPrice,
      canOrder: canOrder,
      totalIngredients: newQuantity,
      calories: newCalories
    };
  };

  order = () => {
    this.setState({
      ordered: true
    });
  };

  cancel = () => {
    this.setState({
      ordered: false
    });
  };

  render() {
    return (
      <>
        {this.state.orderCompleted ? (
          <Redirect
            push
            to={{
              pathname: "/builder/checkout",
              state: {
                ingredients: this.state.ingredients,
                price: this.state.price
              }
            }}
          />
        ) : null}
        <Modal show={this.state.ordered} cancel={this.cancel}>
          {this.state.loading ? (
            <Spinner />
          ) : (
            <OrderSummary
              ingredients={this.state.ingredients}
              cancel={this.cancel}
              price={this.state.price}
              orderBurger={this.orderBurger}
            />
          )}
        </Modal>
        <div
          style={{
            pointerEvents: this.state.ordered ? "none" : "auto"
          }}
        >
          <Burger
            ingredients={this.state.ingredients}
            orderable={this.state.canOrder}
          />
          <BuildControls
            orderable={this.state.canOrder}
            ingredients={this.state.ingredients}
            updateQuantity={this.updateQuantity}
            price={this.state.price}
            calories={this.state.calories}
            order={this.order}
          />
        </div>
      </>
    );
  }
}

export default withErrorHandler(BurgerBuilder, axios);
