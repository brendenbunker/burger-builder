import React, { Component } from "react";
import axios from "../../axiosOrderer";
import withErrorHandler from "../../HOC/withErrorHandler/withErrorHandler";
import classes from "./Checkout.module.css";
import Burger from "../../Component/Burger/Burger";
import Input from "../../Component/UI/Input/Input";
import { Redirect } from "react-router-dom";

class Checkout extends Component {
  state = {
    orderCompleted: false,
    orderCancelled: false,
    name: "",
    address: "",
    zip: "",
    email: ""
  };

  updateHandler = value => e => {
    this.setState({
      [value]: e.target.value
    });
  };

  orderBurger = () => {
    this.setState({ loading: true });
    var today = new Date();
    let order = {
      date: today,
      price: this.props.location.state.price,
      ingredients: this.props.location.state.ingredients,
      customer: {
        name: this.state.name,
        address: this.state.address,
        email: this.state.email,
        zip: this.state.zip
      }
    };

    axios
      .post("/orders.json", order)
      .then(response => {
        this.setState({ orderCompleted: true });
      })
      .catch(error => console.log(error));
  };

  render() {
    return (
      <div className={classes.Checkout}>
        {this.state.orderCancelled ? <Redirect to="/builder" /> : null}
        {!this.state.orderCompleted ? (
          <>
            <form className={classes.CheckoutForm}>
              <Input
                type="text"
                name="name"
                value={this.state.name}
                onChange={this.updateHandler("name")}
              />
              <Input
                type="email"
                name="email"
                value={this.state.email}
                onChange={this.updateHandler("email")}
              />
              <Input
                type="text"
                name="address"
                value={this.state.address}
                onChange={this.updateHandler("address")}
              />
              <Input
                type="text"
                name="zip"
                value={this.state.zip}
                onChange={this.updateHandler("zip")}
              />
            </form>
            <div className={classes.ButtonContainer}>
              <button onClick={this.orderBurger}>Yes</button>
              <button onClick={this.props.cancel}>No</button>
            </div>
          </>
        ) : (
          <>
            <h1>Thanks for Ordering</h1>
            <Burger
              ingredients={this.props.location.state.ingredients}
              price={this.state.price}
              orderable={this.state.canOrder}
            />
          </>
        )}
      </div>
    );
  }
}

export default withErrorHandler(Checkout, axios);
