import React, { Component } from "react";
import axios from "../../axiosOrderer";
import Spinner from "../../Component/UI/Spinner/Spinner";
import OrderedItem from "../../Component/OrderedItem/OrderedItem.js";
import classes from "./OrderHistory.module.css";

class OrderHistory extends Component {
  state = { orders: null, loading: true };

  componentDidMount() {
    axios.get("/orders.json").then(res => {
      var obj = res.data;
      var result = Object.keys(obj).map(function(key) {
        return { key: key, ...obj[key] };
      });
      this.setState({ orders: result });
    });
  }

  render() {
    return !this.state.orders ? (
      <Spinner />
    ) : (
      <div className={classes.OrderHistory}>
        <h1>Order History</h1>
        {this.state.orders.reverse().map(order => {
          let calories = order.ingredients.reduce((pv, cv) => {
            return pv + cv.calories * cv.quantity;
          }, 0);
          let price = order.ingredients.reduce((pv, cv) => {
            return pv + cv.price * cv.quantity;
          }, 0);
          return (
            <OrderedItem
              key={order.key}
              price={price}
              calories={calories}
              ingredients={order.ingredients}
              date={order.date}
            />
          );
        })}
      </div>
    );
  }
}

export default OrderHistory;
