import React, { Component } from "react";

import Modal from "../../Component/UI/Modal/Modal";
//import modules from "./withErrorHandler.module.css";

const withErrorHandler = (WrappedComponent, axios) => {
  return class extends Component {
    state = {
      error: null
    };

    componentDidMount() {
      this.reqInt = axios.interceptors.request.use(
        res => res,
        err => {
          this.setState({ error: err });
        }
      );

      this.resInt = axios.interceptors.response.use(
        res => res,
        err => {
          this.setState({ error: err });
        }
      );
    }

    componentWillUnmount() {
      axios.interceptors.request.eject(this.reqInt);
      axios.interceptors.response.eject(this.resInt);
    }

    awknowledged = () => {
      this.setState({ error: null });
    };

    render() {
      return (
        <>
          {this.state.error === null ? null : (
            <Modal show={this.state.error} cancel={this.awknowledged}>
              {this.state.error.message ? this.state.error.message : null}
            </Modal>
          )}
          <WrappedComponent {...this.props} />
        </>
      );
    }
  };
};

export default withErrorHandler;
